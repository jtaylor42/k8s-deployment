# k8s deployment

Full setup to deliver a service-meshed, network-segregated k8s cluster deployment.

| **Section** | **Description** |
| --- | ----- |
| [Overview](#overview) | Brief description of what is being built |
| [Build](#the-infrastructure) | Create all LXC containers needed for the k8s deployment |
| [Preparation](#prep-infrastructure) | Initial config of proxy and services and create/disperse all certs/keys/ca |
| [Configuration](#configure-infrastrucuture) | Bootstrap the controller and worker nodes and install etcd |
| [Remote Access](#'setup-remote-access') | Setup access from outside the cluster via the HA Proxy |


## Overview

Full setup to deliver a service-meshed, network-segregated k8s cluster deployment based on the "Kubernetes the hard way" guide and then going beyond that guide and into the service mesh, serverless deployment, a CNI, etc.

This document covers end-to-end installation and setup of k8s and all dependencies including docker, various utilities and networking overlay. Follow on documents will delve into more advanced Calico network configuration as well as service mesh use.

![What it looks like](img/overview.png "K8S Plan")

## Installation

The installation of k8s shares many requirements on all nodes (master and worker). After all of the common installation is done, the [configuration](#configuration) section will initialize the cluster on the master node, create the networking overlay and then join the worker node(s) to the cluster.

#### Prerequisites

Swap needs to be disabled on the host system.
LXD needs to be installed.
This repo needs to be cloned and please navigate to the 'files' directory.

### The infrastrcture

This is eventually be fully deployed via ansible/terraform. For now, this cluster is deployed manually using LXC containers as the nodes. There are 3 controller nodes and 3 worker nodes with "HA Proxy" as the load balancer providing access to the cluster. At the moment, the HA Proxy is not redundant and the etcd datastores are all located on the controller nodes, but this will get fixed with future iterations.

| **Node** | **Description** |
| --- | ----- |
| controller-0 | ip address and pod network cidr |
| controller-1 | ip address and pod network cidr |
| controller-2 | ip address and pod network cidr |
| worker-0 | ip address and pod network cidr |
| worker-1 | ip address and pod network cidr |
| worker-2 | ip address and pod network cidr |
| haproxy | ip address and pod network cidr |

```python
lxc launch images:centos/7 haproxy
for i in 0 1 2; do
 lxc launch ubuntu:18.04 controller-$i --profile k8s
 lxc launch ubuntu:18.04 worker-$i --profile k8s
done
# the k8s profile is just one I created to limit cpu/mem usage per image
# it is optional especially if there will not be a huge load on the cluster
```

### Prep infrastructure

Again, this will be automated in the future. Just fully spelled out here for now. Prepping infrastrcture includes initial configuration of the HA Proxy, installing dependencies and genarating and provisioning the CA and all of the certifications.

All files to be used to cert generation and various configurations are available via my public git repo (this one). I plan on cleaning it up a bit later. Lots to do "later".

#### HA Proxy configuration

The haproxy.cfg file is in the repo and currently needs to manually be modified with the controller and ha proxy IP addresses. This is easy enough to automate later.

```python
lxc exec haproxy -- yum install -y haproxy net-tools
lxc file push haproxy.cfg haproxy/etc/haproxy/haproxy.cfg
lxc exec haproxy -- systemctl enable haproxy
lxc exec haproxy -- systemctl start haproxy
```

#### Install required client tools

First is on the linux host (Ubuntu 18.04 bare metal desktop server) hosting the non-clustered LXD environment. 

```python
# cfssl and cfssljson binaries
wget -q --show-progress --https-only --timestamping \
  https://storage.googleapis.com/kubernetes-the-hard-way/cfssl/linux/cfssl \
  https://storage.googleapis.com/kubernetes-the-hard-way/cfssl/linux/cfssljson
chmod +x cfssl cfssljson
sudo mv cfssl cfssljson /usr/local/bin/

# kubectl
wget https://storage.googleapis.com/kubernetes-release/release/v1.15.3/bin/linux/amd64/kubectl
chmod +x kubectl
sudo mv kubectl /usr/local/bin/

# provision CA and generate certificates
cfssl gencert -initca ca-csr.json | cfssljson -bare ca # provision CA

cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=kubernetes \
  admin-csr.json | cfssljson -bare admin # generate admin cert/key

for instance in worker-0 worker-1 worker-2; do
EXTERNAL_IP=$(lxc info ${instance} | grep eth0 | head -1 | awk '{print $3}')
cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json \
  -hostname=${instance},${EXTERNAL_IP} -profile=kubernetes ${instance}-csr.json | cfssljson -bare ${instance}
done #generates the certs/keys for worker nodes

cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=kubernetes \
  kube-controller-manager-csr.json | cfssljson -bare kube-controller-manager # controller manager cert/key

cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=kubernetes \
  kube-proxy-csr.json | cfssljson -bare kube-proxy # kube proxy cert/key

cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=kubernetes \
  kube-scheduler-csr.json | cfssljson -bare kube-scheduler # kube scheduler cert/key

HA_IP=$(lxc info haproxy | grep eth0 | head -1 | awk '{print $3}')
C0_IP=$(lxc info controller-0 | grep eth0 | head -1 | awk '{print $3}')
C1_IP=$(lxc info controller-1 | grep eth0 | head -1 | awk '{print $3}')
C2_IP=$(lxc info controller-2 | grep eth0 | head -1 | awk '{print $3}')
KUBERNETES_HOSTNAMES=kubernetes,kubernetes.default,kubernetes.default.svc,kubernetes.default.svc.cluster,kubernetes.svc.cluster.local
cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json \
  -hostname=10.32.0.1,${C0_IP},${C1_IP},${C2_IP},${HA_IP},127.0.0.1,${KUBERNETES_HOSTNAMES} \
  -profile=kubernetes kubernetes-csr.json | cfssljson -bare kubernetes # create variables to be used to generate a cert for haproxy and controllers

cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=kubernetes \
  service-account-csr.json | cfssljson -bare service-account # service account cert/key

for instance in worker-0 worker-1 worker-2; do
  lxc file push ca.pem ${instance}-key.pem ${instance}.pem ${instance}/root/
done #copies the certs/key/ca to the worker nodes

for instance in controller-0 controller-1 controller-2; do
  lxc file push ca.pem ca-key.pem kubernetes-key.pem kubernetes.pem \
    service-account-key.pem service-account.pem ${instance}/root/
done # copies certs/keys/ca for controller nodes

for instance in worker-0 worker-1 worker-2; do
  kubectl config set-cluster kubernetes-the-hard-way --certificate-authority=ca.pem --embed-certs=true \
    --server=https://${HA_IP}:6443 --kubeconfig=${instance}.kubeconfig
  kubectl config set-credentials system:node:${instance} --client-certificate=${instance}.pem \
    --client-key=${instance}-key.pem --embed-certs=true --kubeconfig=${instance}.kubeconfig
  kubectl config set-context default --cluster=kubernetes-the-hard-way --user=system:node:${instance} \
    --kubeconfig=${instance}.kubeconfig
  kubectl config use-context default --kubeconfig=${instance}.kubeconfig
done # generate the kubeconfig files for the worker nodes

{
  kubectl config set-cluster kubernetes-the-hard-way --certificate-authority=ca.pem --embed-certs=true \
    --server=https://${HA_IP}:6443 --kubeconfig=kube-proxy.kubeconfig
  kubectl config set-credentials system:kube-proxy --client-certificate=kube-proxy.pem \
    --client-key=kube-proxy-key.pem --embed-certs=true --kubeconfig=kube-proxy.kubeconfig
  kubectl config set-context default --cluster=kubernetes-the-hard-way \
    --user=system:kube-proxy --kubeconfig=kube-proxy.kubeconfig
  kubectl config use-context default --kubeconfig=kube-proxy.kubeconfig
}# generate kubeconfig for the proxy

{
  kubectl config set-cluster kubernetes-the-hard-way --certificate-authority=ca.pem --embed-certs=true \
    --server=https://127.0.0.1:6443 --kubeconfig=kube-controller-manager.kubeconfig
  kubectl config set-credentials system:kube-controller-manager --client-certificate=kube-controller-manager.pem \
    --client-key=kube-controller-manager-key.pem --embed-certs=true \
    --kubeconfig=kube-controller-manager.kubeconfig
  kubectl config set-context default --cluster=kubernetes-the-hard-way --user=system:kube-controller-manager \
    --kubeconfig=kube-controller-manager.kubeconfig
  kubectl config use-context default --kubeconfig=kube-controller-manager.kubeconfig
} # kubeconfig for the controller manager

{
  kubectl config set-cluster kubernetes-the-hard-way --certificate-authority=ca.pem --embed-certs=true \
    --server=https://127.0.0.1:6443 --kubeconfig=kube-scheduler.kubeconfig
  kubectl config set-credentials system:kube-scheduler --client-certificate=kube-scheduler.pem \
    --client-key=kube-scheduler-key.pem --embed-certs=true --kubeconfig=kube-scheduler.kubeconfig
  kubectl config set-context default --cluster=kubernetes-the-hard-way --user=system:kube-scheduler \
    --kubeconfig=kube-scheduler.kubeconfig
  kubectl config use-context default --kubeconfig=kube-scheduler.kubeconfig
} # kubeconfig for the scheduler

{
  kubectl config set-cluster kubernetes-the-hard-way --certificate-authority=ca.pem --embed-certs=true \
    --server=https://127.0.0.1:6443 --kubeconfig=admin.kubeconfig
  kubectl config set-credentials admin --client-certificate=admin.pem --client-key=admin-key.pem \
    --embed-certs=true --kubeconfig=admin.kubeconfig
  kubectl config set-context default --cluster=kubernetes-the-hard-way \
    --user=admin --kubeconfig=admin.kubeconfig
  kubectl config use-context default --kubeconfig=admin.kubeconfig
} #kubeconfig for admin

for instance in worker-0 worker-1 worker-2; do
  lxc file push ${instance}.kubeconfig kube-proxy.kubeconfig ${instance}/root/
  lxc config device add ${instance} "kmsg" unix-char source="/dev/kmsg" path="/dev/kmsg" # workaround for kmsg lxc error
done
for instance in controller-0 controller-1 controller-2; do
  lxc file push admin.kubeconfig kube-controller-manager.kubeconfig kube-scheduler.kubeconfig ${instance}/root/
done # push all kubeconfigs out to workers and controllers

ENCRYPTION_KEY=$(head -c 32 /dev/urandom | base64)
cat > encryption-config.yaml <<EOF
kind: EncryptionConfig
apiVersion: v1
resources:
  - resources:
      - secrets
    providers:
      - aescbc:
          keys:
            - name: key1
              secret: ${ENCRYPTION_KEY}
      - identity: {}
EOF
for instance in controller-0 controller-1 controller-2; do
  lxc file push encryption-config.yaml ${instance}/root/
done # generate encryption key and disseminate to the controller nodes
```

### Configure infrastructure

#### Bootstrap the etcd cluster members

This ideally would have the etcd function separate of the controller so as to have high availability of each service independent of one another. For now, they share a node.

```python
wget -q --show-progress --https-only --timestamping \
  "https://github.com/etcd-io/etcd/releases/download/v3.4.0/etcd-v3.4.0-linux-amd64.tar.gz"

for instance in controller-0 controller-1 controller-2; do
  lxc file push etcd-v3.4.0-linux-amd64.tar.gz ${instance}/root/
  lxc exec ${instance} -- tar -xvf etcd-v3.4.0-linux-amd64.tar.gz
  lxc exec ${instance} -- mv /root/etcd-v3.4.0-linux-amd64/etcd* /usr/local/bin
  lxc exec ${instance} -- mkdir -p /etc/etcd /var/lib/etcd
  lxc exec ${instance} -- cp /root/ca.pem /root/kubernetes-key.pem /root/kubernetes.pem /etc/etcd/
  lxc file push ${instance}.etcd.service ${instance}/etc/systemd/system/etcd.service # needs manual updating of IPs for now
  lxc exec ${instance} -- systemctl daemon-reload
  lxc exec ${instance} -- systemctl enable etcd
  lxc exec ${instance} -- systemctl start etcd # hangs up on first host, needs others to come up successfully
done
```

#### Bootstrap k8s control plane

```python
wget -q --show-progress --https-only --timestamping \
  "https://storage.googleapis.com/kubernetes-release/release/v1.15.3/bin/linux/amd64/kube-apiserver" \
  "https://storage.googleapis.com/kubernetes-release/release/v1.15.3/bin/linux/amd64/kube-controller-manager" \
  "https://storage.googleapis.com/kubernetes-release/release/v1.15.3/bin/linux/amd64/kube-scheduler" \
  "https://storage.googleapis.com/kubernetes-release/release/v1.15.3/bin/linux/amd64/kubectl"
for instance in controller-0 controller-1 controller-2; do
  lxc exec ${instance} -- mkdir -p /root/k8sbins
  lxc file push kube-apiserver ${instance}/root/k8sbins/
  lxc file push kube-controller-manager ${instance}/root/k8sbins/
  lxc file push kube-scheduler ${instance}/root/k8sbins/
  lxc file push kubectl ${instance}/root/k8sbins/
  lxc exec ${instance} -- mkdir -p /etc/kubernetes/config
  lxc exec ${instance} -- chmod +x -R /root/k8sbins/
  lxc exec ${instance} -- mv /root/k8sbins/kube-apiserver /usr/local/bin
  lxc exec ${instance} -- mv /root/k8sbins/kube-controller-manager /usr/local/bin
  lxc exec ${instance} -- mv /root/k8sbins/kube-scheduler /usr/local/bin
  lxc exec ${instance} -- mv /root/k8sbins/kubectl /usr/local/bin
  lxc exec ${instance} -- mkdir -p /var/lib/kubernetes/
  lxc exec ${instance} -- mv /root/*.pem /var/lib/kubernetes/ && mv /root/encryption-config.yaml /var/lib/kubernetes/
  lxc exec ${instance} -- mv /root/encryption-config.yaml /var/lib/kubernetes/
done # this would be so much better scripted/automated via ansible. Just downloaded, moving, adjusting privileges of binaries

for instance in controller-0 controller-1 controller-2; do
  lxc file push ${instance}.kube-apiserver.service ${instance}/etc/systemd/system/kube-apiserver.service # needs manual updating of IPs for now
  lxc file push kube-controller-manager.service ${instance}/etc/systemd/system/kube-controller-manager.service
  lxc file push kube-scheduler.yaml ${instance}/etc/kubernetes/config/kube-scheduler.yaml
  lxc file push kube-scheduler.service ${instance}/etc/systemd/system/kube-scheduler.service
  lxc exec ${instance} -- mv /root/kube-controller-manager.kubeconfig /var/lib/kubernetes/
  lxc exec ${instance} -- mv /root/kube-scheduler.kubeconfig /var/lib/kubernetes/
{
  lxc exec ${instance} -- systemctl daemon-reload
  lxc exec ${instance} -- systemctl enable kube-apiserver kube-controller-manager kube-scheduler
  lxc exec ${instance} -- systemctl start kube-apiserver kube-controller-manager kube-scheduler
}
done # push the various service files for the controllers and start them

# this is a good point to check the current status of our cluster
# login to one of the controllers (lxc exec controller-0 bash)
# run: kubectl get componentstatuses --kubeconfig admin.kubeconfig
# should get all etcd nodes, controller manager and scheduler as "healthy"

lxc file push rbac_auth.yml controller-0/root/
lxc file push rbac_bind.yml controller-0/root/
lxc exec controller-0 -- kubectl apply --kubeconfig /root/admin.kubeconfig -f /root/rbac_auth.yml
lxc exec controller-0 -- kubectl apply --kubeconfig /root/admin.kubeconfig -f /root/rbac_bind.yml
# creates and binds rbac authorization for the 'kubernetes' user
```

#### Bootstrap the worker nodes

```python
wget -q --show-progress --https-only --timestamping \
  https://github.com/kubernetes-sigs/cri-tools/releases/download/v1.15.0/crictl-v1.15.0-linux-amd64.tar.gz \
  https://github.com/opencontainers/runc/releases/download/v1.0.0-rc8/runc.amd64 \
  https://github.com/containernetworking/plugins/releases/download/v0.8.2/cni-plugins-linux-amd64-v0.8.2.tgz \
  https://github.com/containerd/containerd/releases/download/v1.2.9/containerd-1.2.9.linux-amd64.tar.gz \
  https://storage.googleapis.com/kubernetes-release/release/v1.15.3/bin/linux/amd64/kube-proxy \
  https://storage.googleapis.com/kubernetes-release/release/v1.15.3/bin/linux/amd64/kubelet
for instance in worker-0 worker-1 worker-2; do
  lxc exec ${instance} -- mkdir -p /etc/cni/net.d /opt/cni/bin /var/lib/kubelet /var/lib/kube-proxy /var/lib/kubernetes /var/run/kubernetes /root/containerd /etc/containerd
  lxc file push crictl-v1.15.0-linux-amd64.tar.gz ${instance}/root/
  lxc file push runc.amd64 ${instance}/root/
  lxc file push cni-plugins-linux-amd64-v0.8.2.tgz ${instance}/root/
  lxc file push containerd-1.2.9.linux-amd64.tar.gz ${instance}/root/
  lxc file push kube-proxy ${instance}/root/
  lxc file push kubelet ${instance}/root/
  lxc file push kubectl ${instance}/root/ # was already downloaded in a previous step
  lxc file push ${instance}.10-bridge.conf ${instance}/etc/cni/net.d/10-bridge.conf
  lxc file push 99-loopback.conf ${instance}/etc/cni/net.d/
  lxc file push config.toml ${instance}/etc/containerd/
  lxc file push containerd.service ${instance}/etc/systemd/system/
  lxc file push ${instance}.kubelet-config.yaml ${instance}/var/lib/kubelet/kubelet-config.yaml
  lxc file push kubelet.service ${instance}/etc/systemd/system/
  lxc file push kube-proxy-config.yaml ${instance}/var/lib/kube-proxy/
  lxc file push kube-proxy.service ${instance}/etc/systemd/system/
  lxc exec ${instance} -- apt-get update
  lxc exec ${instance} -- apt-get install -y socat conntrack ipset
  lxc exec ${instance} -- tar -xvf /root/crictl-v1.15.0-linux-amd64.tar.gz
  lxc exec ${instance} -- tar -xvf /root/containerd-1.2.9.linux-amd64.tar.gz -C /root/containerd/
  lxc exec ${instance} -- tar -xvf /root/cni-plugins-linux-amd64-v0.8.2.tgz -C /opt/cni/bin/
  lxc exec ${instance} -- mv /root/runc.amd64 /root/runc
  lxc exec ${instance} -- chmod +x /root/crictl /root/kubectl /root/kube-proxy /root/kubelet /root/runc
  lxc exec ${instance} -- mv /root/crictl /root/kubectl /root/kube-proxy /root/kubelet /root/runc /usr/local/bin/
  lxc exec ${instance} -- mv /root/containerd/bin/containerd-shim-runc-v1 /bin/
  lxc exec ${instance} -- mv /root/containerd/bin/ctr /bin/
  lxc exec ${instance} -- mv /root/containerd/bin/containerd /bin/
  lxc exec ${instance} -- mv /root/containerd/bin/containerd-stress /bin/
  lxc exec ${instance} -- mv /root/containerd/bin/containerd-shim /bin/
  lxc exec ${instance} -- mv /root/${instance}-key.pem ${instance}.pem /var/lib/kubelet/
  lxc exec ${instance} -- mv /root/${instance}.kubeconfig /var/lib/kubelet/kubeconfig
  lxc exec ${instance} -- mv /root/ca.pem /var/lib/kubernetes
  lxc exec ${instance} -- mv /root/kube-proxy.kubeconfig /var/lib/kube-proxy/kubeconfig
  lxc exec ${instance} -- systemctl daemon-reload
  lxc exec ${instance} -- systemctl enable containerd kubelet kube-proxy
  lxc exec ${instance} -- systemctl start containerd kubelet kube-proxy
done # this mostly is pushing pre-made configuration files that are default with exception of 10-bridge and
     # kubelet-config which needed hostname/IP Address updates manually

# this is another good time to check the status of our cluster
# specifically the worker nodes. Run the following command on the host:
# lxc exec controller-0 -- kubectl get nodes --kubeconfig /root/admin.kubeconfig
# the result should show your three worker nodes in the "Ready" state

```

### Setup remote access

This will allow us to control our cluster from our host machine via interaction with the HA Proxy.

```python
sudo route add -net 10.200.0.0 netmask 255.255.255.0 gw 10.131.52.95
sudo route add -net 10.200.1.0 netmask 255.255.255.0 gw 10.131.52.115
sudo route add -net 10.200.2.0 netmask 255.255.255.0 gw 10.131.52.48
# add routes on the host machine to facilitate routes between LXC nodes

mkdir ~/.kube
lxc file pull controller-0/root/admin.kubeconfig ~/.kube/config
sed -i "s/.*https:\/\/127.0.0.1:6443/    server: https:\/\/$HA_IP:6443/" ~/.kube/config
# sed command simply replaces the loopback address with the HA Proxy address
# good idea to NOT use the -i parameter first to make sure the change works

# now we can test the access to the cluster and verify the cluster is healthy and ready
kubectl cluster-info
kubectl get nodes
kubectl cs
# all should show Running/Ready/Healthy
```

## EoF

That sums it up for "k8s the hard way" with LXC containers on a single physical host.
The next page will cover manually installing other resources for our cluster including service mesh (Istio), a better network interface (Calico), a serverless service (kubeless or knative... not sure yet, probably knative since CCSO explicitly mentions it in their slides), and some monitoring tools built into Istio.

I think my goal is to get a full cluster with various tools all installed by hand before worrying about automation.

Next: [K8s Service Addons](service_addons.md)



