# k8s deployment

This repository is just documenting my various excursions into the field of kubernetes including a few different "the hard way" tutorials as well as more advanced kubernetes features such as installing and configuring Container Network Interface (CNI) plugins, service mesh and serverless solutions as well as trying out various monitoring tools.

Below is a table that will simply link you to the various readmes covering an array of topics.

| **Topic** | **Description** |
| --- | ----- |
| [k8s the hard way - LXC](01-k8s-hard-way-lxc.md) | Step by step walkthrough of installing a k8s cluster with HA proxy |
| [k8s the hard way - AWS](02-k8s-hard-way-aws.md) | Attempting to largely do the same as the 1st tutorial but with AWS |


Next: [k8s the hard way - LXC](01-k8s-hard-way-lxc.md)



