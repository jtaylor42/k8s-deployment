# K8s Service Addons

After the cluster has been deployed ([k8s deploy](README.md)), we can now install the service mesh, container network interface (CNI) and serverless implementation.

| **Section** | **Description** |
| --- | ----- |
| [Service Mesh](#service-mesh) | Installing and initial configuration of Istio |
| [Network](#network-overlay) | Deploying Calico |
| [Serverless](#serverless) | KNative for our serverless solution |

There is 'services' directory where you can navigate to for all of the files I used for these deployments.

## Infrastructure manual provisioning

May have to uninstall HAProxy or limit my istio deployment.

Need to download the latest version of Istio (curl -L https://istio.io/downloadIstio | sh -) and move the istioctl binary file to /usr/local/bin. I currently have 1.6.2 which is included in this repo.

```python
aws ec2 run-instances --image-id ami-085925f297f89fce1 --count 1 --instance-type t2.micro \
 --key-name jtdefaultkp-june2020 --security-group-ids sg-066ff7650e8c041bf --subnet-id \
 subnet-5c4d8370 --no-associate-public-ip-address

```
