#!/bin/bash

key=$(cat terraform/variables.tf | grep -A 4 pubkey | grep default | grep -o '".*"' | sed 's/"//g')
name=$(cat terraform/variables.tf | grep -A 4 project | grep default | grep -o '".*"' | sed 's/"//g')
counter=0


# aws cli command to parse all new instances made by terraform
# and provide the node names and new private ip addresses
aws ec2 describe-instances --filter "Name=instance-state-name,Values=running" --query "Reservations[*].Instances[*].[PrivateIpAddress, Tags[?Key=='Name'].Value|[0]]" --output text | grep k8s >> ec2results.txt


while read -r ipadd col2
do
    node=$(echo "${col2}" | sed "s/${name}-//g")_$counter
#    echo "${node} = ${ipadd}"
    # set local global variable to populate hosts file after loop
    export ${node}=${ipadd}

    # populate the host_vars files
    echo "name: ${node}" > ansible/inventories/host_vars/${ipadd}.yml
    if [[ ${node} == worker* ]]
    then
        echo "lan: 10.210.${counter}.0" >> ansible/inventories/host_vars/${ipadd}.yml
    fi

    counter=$(( $counter + 1 ))
    # just a counter reset to always have 0-2 for types of nodes (etcd, master, worker)
    if [[ $counter == 3 ]]
    then counter=0
    fi
done <ec2results.txt
#echo "$key"

# populate hosts file
echo "[ETCD]" > ansible/inventories/hosts
echo "$etcd_0" >> ansible/inventories/hosts
echo "$etcd_1" >> ansible/inventories/hosts
echo "$etcd_2" >> ansible/inventories/hosts
echo "[MASTER]" >> ansible/inventories/hosts
echo "$master_0" >> ansible/inventories/hosts
echo "$master_1" >> ansible/inventories/hosts
echo "$master_2" >> ansible/inventories/hosts
echo "[WORKER]" >> ansible/inventories/hosts
echo "$worker_0" >> ansible/inventories/hosts
echo "$worker_1" >> ansible/inventories/hosts
echo "$worker_2" >> ansible/inventories/hosts

# populate group_vars file
echo "ansible_ssh_private_key_file: $key" > ansible/inventories/group_vars/all

rm ec2results.txt
