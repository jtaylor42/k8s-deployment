# Provider info
provider "aws" {
  version = ">=2.14"
  region  = var.aws_region
#  profile = var.aws_profile
}

# Data sources
## Ubuntu AMI for all K8s instances
data "aws_ami" "ubuntu" {
  most_recent = true
#  owners      = ["823543435485"] # should make this a variable or see if it is needed at all
  owners      = ["099720109477"] # should make this a variable or see if it is needed at all

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }
}


## launch ec2 instances
## etcd
resource "aws_launch_configuration" "etcd" {
  name_prefix                 = "etcd-"
  image_id                    = data.aws_ami.ubuntu.id
  instance_type               = var.ec2_instance_type
  security_groups             = [var.sec_group_id]
#  security_groups             = "sg-066ff7650e8c041bf"
  key_name                    = var.aws_key_pair_name
  ebs_optimized               = false
  enable_monitoring           = false
  associate_public_ip_address = false

  lifecycle {
    create_before_destroy = true
  }
}

## Kubernetes Master
resource "aws_launch_configuration" "master" {
  name_prefix                 = "master-"
  image_id                    = data.aws_ami.ubuntu.id
  instance_type               = var.ec2_instance_type
  security_groups             = [var.sec_group_id]
#  security_groups             = "sg-066ff7650e8c041bf"
  key_name                    = var.aws_key_pair_name
  associate_public_ip_address = false
  ebs_optimized               = false
  enable_monitoring           = false

  lifecycle {
    create_before_destroy = true
  }
}

## Kubernetes Worker
resource "aws_launch_configuration" "worker" {
  name_prefix                 = "worker-"
  image_id                    = data.aws_ami.ubuntu.id
  instance_type               = var.ec2_instance_type
  security_groups             = [var.sec_group_id]
#  security_groups             = "sg-066ff7650e8c041bf"
  key_name                    = var.aws_key_pair_name
  associate_public_ip_address = false
  ebs_optimized               = false
  enable_monitoring           = false

  lifecycle {
    create_before_destroy = true
  }
}

# scaling nodes each to 3
## etcd
resource "aws_autoscaling_group" "etcd" {
  max_size             = 3
  min_size             = 3
  desired_capacity     = 3
  force_delete         = true
  launch_configuration = aws_launch_configuration.etcd.name
#  vpc_zone_identifier  = [var.vpc_id]
  vpc_zone_identifier  = [var.subnet_id]

  tags = [
    {
      key                 = "Name"
      value               = "${var.project}-etcd"
      propagate_at_launch = true
    },
    {
      key                 = "Project"
      value               = var.project
      propagate_at_launch = true
    },
    {
      key                 = "Owner"
      value               = var.owner
      propagate_at_launch = true
    }
  ]
}

## master
resource "aws_autoscaling_group" "master" {
  max_size             = 3
  min_size             = 3
  desired_capacity     = 3
  force_delete         = true
  launch_configuration = aws_launch_configuration.master.name
#  vpc_zone_identifier  = [var.vpc_id]
  vpc_zone_identifier  = [var.subnet_id]

  tags = [
    {
      key                 = "Name"
      value               = "${var.project}-master"
      propagate_at_launch = true
    },
    {
      key                 = "Project"
      value               = var.project
      propagate_at_launch = true
    },
    {
      key                 = "Owner"
      value               = var.owner
      propagate_at_launch = true
    }
  ]
}

## worker
resource "aws_autoscaling_group" "worker" {
  max_size             = 3
  min_size             = 3
  desired_capacity     = 3
  force_delete         = true
  launch_configuration = aws_launch_configuration.worker.name
#  vpc_zone_identifier  = [var.vpc_id]
  vpc_zone_identifier  = [var.subnet_id]

  tags = [
    {
      key                 = "Name"
      value               = "${var.project}-worker"
      propagate_at_launch = true
    },
    {
      key                 = "Project"
      value               = var.project
      propagate_at_launch = true
    },
    {
      key                 = "Owner"
      value               = var.owner
      propagate_at_launch = true
    }
  ]
}
