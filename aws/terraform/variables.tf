variable "project" {
  description = "Project name used for tags"
  type        = string
  default     = "k8s"
}

variable "owner" {
  description = "Owner name used for tags"
  type        = string
  default     = "Novetta JT"
}

# variable "stage" {
#   description = "Environment name (e.g. `testing`, `dev`, `staging`, `prod`)"
#   type        = string
#   default     = "testing"
# }
variable "sec_group_id" {
  description = "AWS security group for all instances to deploy to"
  type        = string
  default     = "sg-066ff7650e8c041bf"
}

variable "subnet_id" {
  description = "AWS subnet ID for where the instances should deploy"
  type        = string
  default     = "subnet-5c4d8370"
}

variable "vpc_id" {
  description = "AWS VPC ID for where the instances should deploy"
  type        = string
  default     = "vpc-2be10151"
}

variable "aws_profile" {
  description = "AWS cli profile (e.g. `default`)"
  type        = string
  default     = "default"
}

variable "aws_region" {
  description = "AWS region (e.g. `us-east-1` => US North Virginia)"
  type        = string
  default     = "us-east-1"
}

variable "aws_key_pair_name" {
  description = "AWS Key Pair name to use for EC2 Instances (if already existent)"
  type        = string
  default     = "jtdefaultkp-june2020"
}

#this variable is only used by ansible after terraform completes
#this key needs to be located on the runner somewhere
#just point to local id_rsa.pub if ssh keys setup via that method
variable "pubkey" {
  description = "Local location and name of public key for ansible to use to connect to cluster"
  type        = string
  default     = "/home/ubuntu/jtdefaultkp-june2020.pem"
}

variable "ec2_instance_type" {
  description = "EC2 instance type for Bastion Host"
  type        = string
  default     = "t2.micro"
}
